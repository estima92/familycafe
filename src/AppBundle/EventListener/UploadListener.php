<?php

/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 16.06.2016
 * Time: 14:31
 */

namespace AppBundle\EventListener;
use AppBundle\Entity\Photo;
use Doctrine\Common\Persistence\ObjectManager;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Symfony\Component\HttpFoundation\File\File;

class UploadListener
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function onUpload(PostUploadEvent $event)
    {
        $request = $event->getRequest();

        $file = $event->getFile();

        $uploadedfile = new Photo();

        /**@var File $file */
        $filename = $file->getFilename();
        $uploadedfile->setPath($filename);
        $dishId = $request->get('dishId');
        $dish = $this->om->getRepository('AppBundle:Dish')->find($dishId);
        $uploadedfile->setDish($dish);
        $photocount = $dish->getDishPhotos()->count();
        if ($photocount == 0) {
            $uploadedfile->setPhotoOrder(1);
        }
        else $uploadedfile->setPhotoOrder($photocount + 1);
        $dish->addDishPhoto($uploadedfile);



        $this->om->persist($dish);
        $this->om->flush();


    }
}