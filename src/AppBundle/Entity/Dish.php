<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 08.06.2016
 * Time: 12:02
 */


namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="Dish")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Entity\dishRepository")
 */
class Dish
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $dishName;

    /**
     * @var
     * @ORM\Column(type="text")
     */
    private $dishDescription;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=false)
     */
    private $dishPrice;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=false)
     */
    private $dishWeight;

    
    /**
     * @ORM\ManyToMany(targetEntity="DishCategory", inversedBy="dishes")
     * @ORM\JoinTable(name="DishJoin")
     */
    private $dishCategories;
    public function __construct() {
        $this->dishCategories = new ArrayCollection();
        $this->dishPhotos = new ArrayCollection();
    }

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="dish", cascade={"persist", "remove"})
     */
//* @ORM\JoinTable(name="dish_photos",
//*      joinColumns={@ORM\JoinColumn(name="dish_id", referencedColumnName="id")},
//*      inverseJoinColumns={@ORM\JoinColumn(name="photo_id", referencedColumnName="id", unique=true)}
//     *      )
    private $dishPhotos;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\DishComment", mappedBy="dish", cascade={"persist", "remove"})
     */
    private $dishComments;
    
    

//
//    /**
//     * @var string
//     * @Assert\File( maxSize = "3072k", mimeTypesMessage = "Please upload a valid Image")
//     * @ORM\Column(name="image", type="string", length=245, nullable=false)
//     */
//    private $photo;
//    public function getPhoto()
//    {
//        return $this->photo;
//    }
//    public function setPhoto($photo)
//    {
//        $this->photo = $photo;
//
//        return $this;
//    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dishName
     *
     * @param string $dishName
     *
     * @return Dish
     */
    public function setDishName($dishName)
    {
        $this->dishName = $dishName;

        return $this;
    }

    /**
     * Get dishName
     *
     * @return string
     */
    public function getDishName()
    {
        return $this->dishName;
    }

    /**
     * Set dishDescription
     *
     * @param string $dishDescription
     *
     * @return Dish
     */
    public function setDishDescription($dishDescription)
    {
        $this->dishDescription = $dishDescription;

        return $this;
    }

    /**
     * Get dishDescription
     *
     * @return string
     */
    public function getDishDescription()
    {
        return $this->dishDescription;
    }

    /**
     * Set dishPrice
     *
     * @param integer $dishPrice
     *
     * @return Dish
     */
    public function setDishPrice($dishPrice)
    {
        $this->dishPrice = $dishPrice;

        return $this;
    }

    /**
     * Get dishPrice
     *
     * @return integer
     */
    public function getDishPrice()
    {
        return $this->dishPrice;
    }

    /**
     * Set dishWeight
     *
     * @param integer $dishWeight
     *
     * @return Dish
     */
    public function setDishWeight($dishWeight)
    {
        $this->dishWeight = $dishWeight;

        return $this;
    }

    /**
     * Get dishWeight
     *
     * @return integer
     */
    public function getDishWeight()
    {
        return $this->dishWeight;
    }

    

    /**
     * Add dishCategory
     *
     * @param \AppBundle\Entity\DishCategory $dishCategory
     *
     * @return Dish
     */
    public function addDishCategory(\AppBundle\Entity\DishCategory $dishCategory)
    {
        $this->dishCategories[] = $dishCategory;

        return $this;
    }

    /**
     * Remove dishCategory
     *
     * @param \AppBundle\Entity\DishCategory $dishCategory
     */
    public function removeDishCategory(\AppBundle\Entity\DishCategory $dishCategory)
    {
        $this->dishCategories->removeElement($dishCategory);
    }

    /**
     * Get dishCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDishCategories()
    {
        return $this->dishCategories;
    }

    /**
     * Add dishPhoto
     *
     * @param \AppBundle\Entity\Photo $dishPhoto
     *
     * @return Dish
     */
    public function addDishPhoto(\AppBundle\Entity\Photo $dishPhoto)
    {
        $this->dishPhotos[] = $dishPhoto;

        return $this;
    }

    /**
     * Remove dishPhoto
     *
     * @param \AppBundle\Entity\Photo $dishPhoto
     */
    public function removeDishPhoto(\AppBundle\Entity\Photo $dishPhoto)
    {
        $this->dishPhotos->removeElement($dishPhoto);
    }

    /**
     * Get dishPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDishPhotos()
    {
        return $this->dishPhotos;
    }

    /**
     * Add dishComment
     *
     * @param \AppBundle\Entity\DishComment $dishComment
     *
     * @return Dish
     */
    public function addDishComment(\AppBundle\Entity\DishComment $dishComment)
    {
        $this->dishComments[] = $dishComment;

        return $this;
    }

    /**
     * Remove dishComment
     *
     * @param \AppBundle\Entity\DishComment $dishComment
     */
    public function removeDishComment(\AppBundle\Entity\DishComment $dishComment)
    {
        $this->dishComments->removeElement($dishComment);
    }

    /**
     * Get dishComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDishComments()
    {
        return $this->dishComments;
    }
}
