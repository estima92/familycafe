<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 06.06.2016
 * Time: 12:28
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Task")
 */

class Task
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $task;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=false)
     */
    protected $dueDate;

    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }

    public function getDueDate()
    {
        return $this->dueDate;
    }

    public function setDueDate(\DateTime $dueDate = null)
    {
        $this->dueDate = $dueDate;
    }
    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
