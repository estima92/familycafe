<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 14.06.2016
 * Time: 11:46
 */

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\dishCategoryRepository")
 * @ORM\Table(name="DishCategory")
 *
 */
class DishCategory {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $dishCategoryName;

    

    /**
     * @ORM\ManyToMany(targetEntity="Dish", mappedBy="dishCategories")
     */
    private $dishes;

    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dishCategoryName
     *
     * @param string $dishCategoryName
     *
     * @return DishCategory
     */
    public function setDishCategoryName($dishCategoryName)
    {
        $this->dishCategoryName = $dishCategoryName;

        return $this;
    }

    /**
     * Get dishCategoryName
     *
     * @return string
     */
    public function getDishCategoryName()
    {
        return $this->dishCategoryName;
    }
    

    /**
     * Add dish
     *
     * @param \AppBundle\Entity\Dish $dish
     *
     * @return DishCategory
     */
    public function addDish(\AppBundle\Entity\Dish $dish)
    {
        $this->dishes[] = $dish;

        return $this;
    }

    /**
     * Remove dish
     *
     * @param \AppBundle\Entity\Dish $dish
     */
    public function removeDish(\AppBundle\Entity\Dish $dish)
    {
        $this->dishes->removeElement($dish);
    }

    /**
     * Get dishes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDishes()
    {
        return $this->dishes;
    }
}
