<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 06.06.2016
 * Time: 15:01
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="News")
 */
class News
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $newsHeader;

    /**
     * @ORM\Column(type="text")
     */
    private $newsText;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=false)
     */
    private $Date;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newsHeader
     *
     * @param string $newsHeader
     *
     * @return News
     */
    public function setNewsHeader($newsHeader)
    {
        $this->newsHeader = $newsHeader;

        return $this;
    }

    /**
     * Get newsHeader
     *
     * @return string
     */
    public function getNewsHeader()
    {
        return $this->newsHeader;
    }

    /**
     * Set newsText
     *
     * @param string $newsText
     *
     * @return News
     */
    public function setNewsText($newsText)
    {
        $this->newsText = $newsText;

        return $this;
    }

    /**
     * Get newsText
     *
     * @return string
     */
    public function getNewsText()
    {
        return $this->newsText;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return News
     */
    public function setDate($date)
    {
        $this->Date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->Date;
    }
}
