<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 16.06.2016
 * Time: 22:08
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="DishComment")
 */
class DishComment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *  @ORM\ManyToOne(targetEntity="Dish", inversedBy="dishComments")
     * @ORM\JoinColumn(name="dish_id", referencedColumnName="id")
     */
    private $dish;
    
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $user;
    
    /**
     * @var
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=false)
     */
    private $rating;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $isApproved;

    public function __construct() {
        $this->setIsApproved(false);
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return DishComment
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return DishComment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return DishComment
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set dish
     *
     * @param \AppBundle\Entity\Dish $dish
     *
     * @return DishComment
     */
    public function setDish(\AppBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \AppBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     *
     * @return DishComment
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }
}
