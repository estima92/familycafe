<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;



/**
* @ORM\Entity
* @ORM\Table(name="Photo")
 * @ORM\HasLifecycleCallbacks
*/
class Photo {
/**
* @ORM\Column(type="integer")
* @ORM\Id
* @ORM\GeneratedValue(strategy="AUTO")
*/
private $id;

/**
*  @ORM\manyToOne(targetEntity="Dish", inversedBy="dishPhotos")
* @ORM\JoinColumn(name="dish_id", referencedColumnName="id")
*/
private $dish;

/**
* @ORM\Column(type="string", length=200)
*/
private $path;

/**
 * @ORM\Column(type="integer")
 */
private $photoOrder;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Photo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Photo
     */
    public function setPhotoOrder($photoOrder)
    {
        $this->photoOrder = $photoOrder;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getPhotoOrder()
    {
        return $this->photoOrder;
    }
    public function getAbsolutePath(){
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath(){
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir(){
        // the absolute directory path where uploadeddocuments should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir(){
        // get rid of the __DIR__ so it doesn't screw up when displaying uploaded doc/image in the view.
        return 'uploads/gallery';
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeFile(){
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
        
        
        
        
    }

    /**
     * Set dish
     *
     * @param integer $dish
     *
     * @return Photo
     */
    public function setDish($dish)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return integer
     */
    public function getDish()
    {
        return $this->dish;
    }

    public static function sort($a, $b)
        {
            if ($a->photoOrder == $b->photoOrder) {
                return 0;
            }
            return ($a->photoOrder < $b->photoOrder ? -1 : 1) ;
        }

}
