<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DishComment;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Dish;
use AppBundle\Form\DishType;
use Symfony\Component\HttpFoundation\File;

/**
 * Dish controller.
 *
 * @Route("/admin/dish")
 */
class DishController extends Controller
{
    /**
     * Lists all Dish entities.
     *
     * @Route("/", name="dish_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dishes = $em->getRepository('AppBundle:Dish')->findAll();

        return $this->render('dish/index.html.twig', array(
            'dishes' => $dishes,
        ));
    }

    /**
     * Creates a new Dish entity.
     *
     * @Route("/new", name="dish_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $dish = new Dish();
        $form = $this->createForm('AppBundle\Form\DishType', $dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();

            return $this->redirectToRoute('dish_show', array('id' => $dish->getId()));
        }

        return $this->render('dish/new.html.twig', array(
            'dish' => $dish,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Dish entity.
     *
     * @Route("/{id}", name="dish_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Dish $dish){
        $deleteForm = $this->createDeleteForm($dish);
        $photos = $dish->getDishPhotos()->toArray();
        usort($photos, array("AppBundle\Entity\Photo" , "sort"));
        return $this->render('dish/show.html.twig', array(
            'dish' => $dish,
            'photos' => $photos,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Dish entity.
     *
     * @Route("/{id}/edit", name="dish_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Dish $dish)
    {
        
        $deleteForm = $this->createDeleteForm($dish);
        $editForm = $this->createForm('AppBundle\Form\DishType', $dish);
        $editForm->handleRequest($request);
        $comment = new DishComment();
        $commentForm = $this->createForm('AppBundle\Form\CommentType', $comment);
        $commentForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();

            return $this->redirectToRoute('dish_edit', array('id' => $dish->getId()));
        }
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setDish($dish);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('dish_edit', array('id' => $dish->getId()));
        }
        return $this->render('dish/edit.html.twig', array(
            'dish' => $dish,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'comment_form' => $commentForm->createView()
        ));
    }
    /**
     * Displays a form to edit an existing Dish entity.
     *
     * @Route("/{id}/editPhoto", name="dish_editPhoto")
     * @Method({"GET", "POST"})
     */
    public function editPhotoAction($id) {
        $dish = $this->getDoctrine()->getManager()->getRepository('AppBundle:Dish')->find($id);
//        TODO ОТСОСАТЬ У КРОКОДИЛА И ПРОВЕРИТЬ КАК ДЕЛА
        return $this->render(':dish:editPhoto.html.twig', array('dish'=>$dish));
    }


    /**
     * Deletes a Dish entity.
     *
     * @Route("/{id}", name="dish_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Dish $dish)
    {
        $form = $this->createDeleteForm($dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dish);
            $em->flush();
        }

        return $this->redirectToRoute('dish_index');
    }

    /**
     * Creates a form to delete a Dish entity.
     *
     * @param Dish $dish The Dish entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Dish $dish)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dish_delete', array('id' => $dish->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    

    
}
