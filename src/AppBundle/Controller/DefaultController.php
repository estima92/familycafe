<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DishComment;
use AppBundle\Form\OrderFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage1")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('AppBundle:News');
        $qb = $repository->createQueryBuilder('n');
        $query = $qb->getQuery();
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);
        $newsCount = count($paginator);
        $categories = $em->getRepository('AppBundle:DishCategory')->findAll();
        
        
        return $this->render('cafe.html.twig', array(
            'categories'=>$categories,
            'newsCount'=>$newsCount,
        ));
    }

    /**
     * @Route("/contact", _name="contact")
     *
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\OrderFormType');

//        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject($form->get('subject')->getData())
                    ->setFrom($form->get('email')->getData())
                    ->setTo('estima1992@gmail.com')
                    ->setBody(
                        $this->renderView(
                            'test.html.twig',
                            array(
                                'ip' => $request->getClientIp(),
                                'name' => $form->get('name')->getData(),
                                'message' => $form->get('message')->getData()
                            )
                        )
                    );

                $this->get('mailer')->send($message);

//                $request->getSession()->getFlashBag()->add('success', 'Your email has been sent! Thanks!');

//                return $this->redirect($this->generateUrl('contact'));
                return new Response('<p class="commentformresponse">Спасибо,мы свяжемся с вами в ближайшее время!</p>');
            }
//        }

//        return array(
//            'form' => $form->createView()
//        );
        return $this->render('mail.html.twig', array(
        'form'=>$form->createView(),
        ));
    }

    /**
     * @Route("/getDishByCategory", name="dishCategorized")
     */
    public  function dishCategorizeAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $id = $request->get('id');
        $currentPage = $request->get('currentPage');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Dish');
        $qb = $repository->createQueryBuilder('d')
            ->innerJoin('d.dishCategories', 'cat')
            ->where('cat.id = :cid')
            ->setParameter('cid' , $id);
        $query = $qb->getQuery();
        $dishes = $query->getResult();
//        $dishes = $repository->getDishByCategory($currentPage);
        $webpaths = array();
        foreach ($dishes as $dish) {
            $paths = $dish->getDishPhotos();
            foreach ($paths as $path) {
                $webpath = $path->getWebPath();
                array_unshift($webpaths, $webpath);
            }
        }

//
        $json = array('webpath'=>$webpaths,
            'dishes'=>$dishes,
        );

        $serializedEntity = $serializer->serialize($json, 'json');
        return new JsonResponse($serializedEntity);

    }

    /**
     * @Route("/getCommentForm{id}", name="getCommentForm")
     */
    public  function getCommentFormAction(Request $request, $id){
        $comment = new DishComment();
        $dish = $this->getDoctrine()->getRepository('AppBundle:Dish')->find($id);
        $commentForm = $this->createForm('AppBundle\Form\CommentType', $comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setDish($dish);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return new Response('<p class="commentformresponse">Спасибо,ваш отзыв очень важен для нас!</p>');
        }
        return $this->render('dish/commentForm.html.twig', array(
            'form' => $commentForm->createView(),
            'id'=> $id,
        ));
    }

    /**
     * @Route("/getNews", name="getNews")
     */
    public function getNews(Request $request){

        $newsPage = $request->get('newsPage');
//        if ($newsPage == null){
//
//        }
        $MaxResults = 6;
        $FirstResult = ($newsPage * $MaxResults) - $MaxResults;
        
        $repository = $this->getDoctrine()->getRepository('AppBundle:News');
        $qb = $repository->createQueryBuilder('n')
            ->setFirstResult($FirstResult)
            ->setMaxResults ($MaxResults);
        $query = $qb->getQuery();
        $news = $query->getResult();

        return $this->render('news.html.twig', array(
            'news' => $news,
        ));
    }

    /**
     * @Route("/getDishComments", name="getDishComments")
     */
    public function getDishComments(Request $request)
    {
        $commentsPage = $request->get('commentsPage');
        $dishId = $request->get('dishId');
        $MaxResults = 3;
        $FirstResult = ($commentsPage * $MaxResults) - $MaxResults;

        $repository = $this->getDoctrine()->getRepository('AppBundle:DishComment');
        $qb = $repository->createQueryBuilder('c')
            ->innerJoin('c.dish', 'dish')
            ->where('dish.id = :id')
            ->setParameter('id' , $dishId)
            ->setFirstResult($FirstResult)
            ->setMaxResults($MaxResults);
        $query = $qb->getQuery();
        $comments = $query->getResult();

        return $this->render('dishComment.html.twig', array(
            'comments' => $comments,
        ));
    }
}
