<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Dish;
use AppBundle\Entity\DishComment;
use AppBundle\Entity\News;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;

class AdminController extends Controller
{

    /**
     * @Route("/admin/test", name="newsJson")
     */
    public  function jsonAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $news= $em->getRepository('AppBundle:News')->findAll();
//        findBy(array('id'=>$id));
//
        $serializedEntity = $serializer->serialize($news, 'json');
//
//        $json ='{"news":'.$serializedEntity.'}';
        return new JsonResponse($serializedEntity);
    }

    /**
     * @Route("/admin/testcomment", name="testcomment")
     */
    public  function newCommentAction(Request $request, DishComment $comment) {
        $comment = new DishComment();
        $form = $this->createForm('AppBundle\Form\CommentType', $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('testcomment', array('id' => $comment->getId()));
        }

        return $this->render('dish/new.html.twig', array(
            'comment' => $comment,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/admin/", name="admin")
     */
    public function adminAction(Request $request) {

        return $this->render('admin.html.twig', array());
    }

    /**
     * @Route("/admin/addnews", name="addnews")
     */
    public function addnewsAction(Request $request)
    {

        $news = new News();
        $news->setNewsHeader('Введите заголовок');
        $news->setNewsText('Введите текст');
        $news->setDate(new \DateTime('today'));
        $form = $this->createFormBuilder($news)
            ->add('newsHeader', TextType::class, array('label' => false))
            ->add('NewsText', TextareaType::class, array('attr' => array('cols'=>'100', 'rows'=>'10'),'label' => false))
            ->add('Date', DateType::class, array('label' => false))
            ->add('save', SubmitType::class, array('label' => 'Добавить новость'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();
            $otvet = 'ВСЕ ПОЛУЧИЛОСЬ БРАТИШКА';
            return $this->redirectToRoute('shownews');
        }

        return $this->render('news/new.html.twig', array(
            'form1' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/shownews", name="shownews")
     */
    public function showNewsAction(){
        $em = $this->getDoctrine()->getManager();
        $news= $em->getRepository('AppBundle:News')->findAll();
        return $this->render('news/index.html.twig', array(
            'news' =>$news,
        ));
    }

    /**
     * @Route("/admin/editnews/{id}", name="editNews")
     */
    public function editNewsAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('AppBundle:News')->find($id);
        $form = $this->createFormBuilder($news)
            ->add('newsHeader', TextType::class, array('label' => false))
            ->add('NewsText', TextareaType::class, array('attr' => array('cols'=>'100', 'rows'=>'10'),'label' => false))
            ->add('Date', DateType::class, array('label' => false))
            ->add('save', SubmitType::class, array('label' => 'Изменить новость'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // ... perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();
            $otvet = 'ВСЕ ПОЛУЧИЛОСЬ БРАТИШКА';
            return $this->render(':news:edit.html.twig', array(
                'form1' => $form->createView(), 'otvet'=>$otvet,
            ));
        }

        return $this->render(':news:edit.html.twig', array(
            'form1' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/admin/addDish", name="addDish")
     */
    public function addDishAction(Request $request)
    {
        
        $dish = new Dish();
        $dish->setDishName('Введите название блюда');
        $dish->setDishDescription('Введите текст');
        $dish->setDishPrice('500');
        $dish->setDishWeight('500');

        $form = $this->createFormBuilder($dish)
            ->add('DishName', TextType::class)
            ->add('DishDescription', TextType::class, array('attr' => array('size'=>'255', 'width'=>'300')))
            ->add('DishPrice', IntegerType::class)
            ->add('DishWeight', IntegerType::class)
            ->add('save', SubmitType::class, array('label' => 'Добавить новость'))
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($dish);
            // actually executes the queries (i.e. the INSERT query)
            $em->flush();
            $otvet = 'ВСЕ ПОЛУЧИЛОСЬ БРАТИШКА';
            return $this->render('menu.html.twig', array(
                'form' => $form->createView(), 'otvet'=>$otvet,
            ));
        }

        return $this->render('menu.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/admin/vitrina", name="vitrina")
     */
    public function vitrinaAction(Request $request)
    {

        $em  = $this->getDoctrine()->getManager();
//        $foo  = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:DishCategory')->findAll();
//        $form = $this->createForm('AppBundle\Form\DishCommentType', $comment);
//        $form->handleRequest($request);
        return $this->render('vitrina.html.twig', array(
            'categories'=>$categories,
//            'commentForm' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/createNews", name="createNews")
     */

    public function createNewsAction()
    {
        $serializer = $this->get('jms_serializer');
        for ($i = 0; $i < 30; $i++) {
            $news = new News();
            $news->setNewsHeader('Zagolovok ' . ($i+1));
            $news->setNewsText('Text novosti nomer' . ($i+1));
            $news->setDate(new \DateTime('today'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();
        }
        $em = $this->getDoctrine()->getManager();
        $news= $em->getRepository('AppBundle:News')->findAll();
        $serializedEntity = $serializer->serialize($news, 'json');
        return new JsonResponse($serializedEntity);
    }

   

    /**
     * @Route("/dish/savePhotoOrder", name="savePhotoOrder")
     */
    public function savePhotoOrder(Request $request){
        $newPhotoOrders = $request->get('photoOrders');
        $em = $this->getDoctrine()->getManager();
        foreach ($newPhotoOrders as $newPhotoOrder){
            $id= $newPhotoOrder["id"];
            $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')->find($id);
            $photo->setPhotoOrder($newPhotoOrder["order"]);
            $em->persist($photo);
            $em->flush();
        }
        return new JsonResponse('huy');
    }

    /**
     * @Route("admin/deletePhoto{fileId}", name="dishDeletePhoto")
     */
    public function dishDeletePhoto($fileId) {
        $file = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($fileId);
        $dish = $file->getDish();
        $dishId = $dish->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Photo');
        $qb = $repository->createQueryBuilder('p')
            // P - алиас таблицы фото
            ->innerJoin('p.dish', 'd')
            //подсоед таблицы которая подвязана на ключе dish и даем ей алиас D
            ->where('p.dish = :dishid')
            ->setParameter('dishid' , $dishId)
            ->orderBy('p.photoOrder', 'ASC');
        $query = $qb->getQuery();
        $photos = $query->getResult();
        for($i=0;$i<count($photos);$i++){
            $photos[$i]->setPhotoOrder($i + 1);
            $em->persist($photos[$i]);
        }
        $em->flush();

        return new JsonResponse('chlen');
    }

    /**
     * @Route("admin/deleteComment{id}", name="dishDeleteComment")
     */
    public function dishDeleteComment($id) {
        $comment = $this->getDoctrine()
            ->getRepository('AppBundle:DishComment')
            ->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        
        return new JsonResponse('Deleted');
    }


    /**
     * @Route("/admin/approveComment{id}", name="dishApproveComment")
     */
    public function dishApproveComment($id) {
        $comment = $this->getDoctrine()
            ->getRepository('AppBundle:DishComment')
            ->find($id);
        $isApproved = $comment->getIsApproved();
        if ($isApproved == 0) {$isApproved = 1;}
        else $isApproved = 0;
        $comment->setIsApproved($isApproved);
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        return new JsonResponse($isApproved);
    }

}
