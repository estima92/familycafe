<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\DishCategory;
use AppBundle\Form\DishCategoryType;
use Symfony\Component\HttpFoundation\Response;

/**
 * DishCategory controller.
 *
 * @Route("/admin/dishcategory")
 */
class DishCategoryController extends Controller
{
    /**
     * Lists all DishCategory entities.
     *
     * @Route("/", name="dishcategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $dishCategories = $em->getRepository('AppBundle:DishCategory')->findAll();

        $deleteForms = array();
        foreach ($dishCategories as $dishCategory) {
            $deleteForms[$dishCategory->getId()] = $this->createDeleteForm($dishCategory)->createView();
            
        }


        return $this->render('dishcategory/index.html.twig', array(
            'dishCategories' => $dishCategories,
            'deleteForms' => $deleteForms,
        ));
    }

    /**
     * Creates a new DishCategory entity.
     *
     * @Route("/new", name="dishcategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $dishCategory = new DishCategory();
        $form = $this->createForm('AppBundle\Form\DishCategoryType', $dishCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dishCategory);
            $em->flush();
//            $dishCategories = $em->getRepository('AppBundle:DishCategory')->findAll();
//            return $this->render('dishcategory/index.html.twig', array(
//                'dishCategories' => $dishCategories,
//            ));
            return $this->redirectToRoute('dishcategory_index');
        }

        return $this->render('dishcategory/new.html.twig', array(
            'dishCategory' => $dishCategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a DishCategory entity.
     *
     * @Route("/{id}", name="dishcategory_show")
     * @Method("GET")
     */
    public function showAction(DishCategory $dishCategory)
    {
        $deleteForm = $this->createDeleteForm($dishCategory);

        return $this->render('dishcategory/show.html.twig', array(
            'dishCategory' => $dishCategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing DishCategory entity.
     *
     * @Route("/{id}/edit", name="dishcategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, DishCategory $dishCategory)
    {
        $deleteForm = $this->createDeleteForm($dishCategory);
        $editForm = $this->createForm('AppBundle\Form\DishCategoryType', $dishCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dishCategory);
            $em->flush();
            return $this->redirectToRoute('dishcategory_index');
//            return $this->redirectToRoute('dishcategory_edit', array('id' => $dishCategory->getId()));
        }

        return $this->render('dishcategory/edit.html.twig', array(
            'dishCategory' => $dishCategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a DishCategory entity.
     *
     * @Route("/{id}", name="dishcategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, DishCategory $dishCategory)
    {
        $form = $this->createDeleteForm($dishCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dishCategory);
            $em->flush();
            return $this->redirectToRoute('dishcategory_index');
        }

        return new JsonResponse('235');
    }

    /**
     * Creates a form to delete a DishCategory entity.
     *
     * @param DishCategory $dishCategory The DishCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DishCategory $dishCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dishcategory_delete', array('id' => $dishCategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
