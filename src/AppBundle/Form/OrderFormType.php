<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class OrderFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Как вас зовут?',
                    'pattern'     => '.{2,}' //minlength
                )
            ))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => 'Введите адрес электронной почты'
                )
            ))
            ->add('subject', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Введите ваш телефон для обратной связи',
                    'pattern'     => '.{3,}' //minlength
                )
            ))
            ->add('message', TextAreaType::class, array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 10,
                    'placeholder' => 'Ваш заказ'
                )
            ))
            ->add('save', SubmitType::class, array(
            'label' => 'Отправить'
            ));
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $collectionConstraint = new Collection(array(
            'name' => array(
                new NotBlank(array('message' => 'Имя не может быть пустым')),
                new Length(array('min' => 2))
            ),
            'email' => array(
                new NotBlank(array('message' => 'не может быть пустым')),
                new Email(array('message' => 'Плохое мыло'))
            ),
            'subject' => array(
                new NotBlank(array('message' => 'не может быть пустым')),
                new Length(array('min' => 3))
            ),
            'message' => array(
                new NotBlank(array('message' => 'не может быть пустым')),
                new Length(array('min' => 5))
            )
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName()
    {
        return 'contact';
    }
}