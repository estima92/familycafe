<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dishName', TextType::class,array('label' => 'Наименование'))
            ->add('dishDescription',TextareaType::class,array('label' => 'Описание'))
            ->add('dishPrice', IntegerType::class,array('label' => 'Цена'))
            ->add('dishWeight', IntegerType::class,array('label' => 'Масса'))
            ->add('dishCategories', EntityType::class,array(
                'label' => 'Категории',
                'class' => 'AppBundle:DishCategory',
                'choice_label' => 'dishCategoryName',
                'expanded' => true,
                'multiple' => true,
            ));

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Dish'
        ));
    }
}
