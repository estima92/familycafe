<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishCategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dishCategoryName')
//            ->add('dishes', EntityType::class,array(
//        'label' => 'Блюда',
//        'class' => 'AppBundle:Dish',
//        'choice_label' => 'dishName',
//        'expanded' => true,
//        'multiple' => true,
//        ))
            ->add('save', SubmitType::class, array('label' => 'Сохранить'));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DishCategory'
        ));
    }
}
