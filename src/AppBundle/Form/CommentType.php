<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.06.2016
 * Time: 12:13
 */

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', TextType::class, array(
                'attr' => array('placeholder'=>'Как вас зовут?')
            ))
            ->add('rating', RangeType::class, array(
        'attr' => array(
            'min' => 1,
            'max' => 5,
            'step'=> 1,
            'placeholder'=>'Как вас зовут?'
            )
    ))
            ->add('text', TextAreaType::class, array(
                'attr' => array('placeholder'=>'Мы будем рады, если вы поставите оценку и оставите отзыв о нашем блюде!')
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Отправить'
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DishComment'
        ));
    }
}